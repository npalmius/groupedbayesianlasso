function [predictions, psi_sampled, sigma_squared_sampled, y_bar_sampled] = grouped_bayesian_lasso(X, y, varargin)
% Copyright (c) 2016, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 05-Sep-2016

    assert(numel(X) == numel(y), 'X must have the same number of elements as y.');
    
    training_elements = get_argument('training_elements', {}, varargin{:});
    max_training_elements = get_argument('max_training_elements', [], varargin{:});
    normalise = get_argument({'normalise', 'normalize'}, true, varargin{:});
    
    assert(isempty(training_elements) || isempty(max_training_elements), 'Arguments training_elements and max_training_elements cannot both be provided.')
    
    if ~isempty(training_elements)
        assert(numel(training_elements) == numel(X), 'The number of individual training elements must be the same as the number of individuals.')
        for i = 1:numel(X)
            assert(numel(training_elements{i}) == size(X{i}, 1), 'The number of training elements for individual %i must be the same as the number of data points.', i)
        end
    elseif numel(max_training_elements) > 1
        assert(numel(max_training_elements) == numel(X), 'The number of individual max training elements must be the same as the number of individuals.')
    elseif numel(max_training_elements) == 1
        max_training_elements = repmat(max_training_elements, numel(X), 1);
    else
        max_training_elements = inf(numel(X), 1);
    end
    
    if ~isempty(max_training_elements)
        training_elements = cell(size(X));
        for i = 1:numel(X)
            training_elements{i} = false(size(X{i}, 1), 1);
            training_elements{i}(1:min(size(X{i}, 1), max_training_elements(i))) = true;
        end
    end
    
    %% Set up parameters
    sweeps = get_argument('sweeps', 500, varargin{:});
    
    lambda_psi = get_argument('lambda_psi', 1, varargin{:});

    % Parameters of sigma_squared prior.
    sigma_squared_a = get_argument('sigma_squared_a', 0, varargin{:});
    sigma_squared_gamma = get_argument('sigma_squared_gamma', 0, varargin{:});

    % Parameters of y_bar prior.
    y_bar_mu = get_argument('y_bar_mu', 0, varargin{:});
    y_bar_sigma = get_argument('y_bar_sigma', 1, varargin{:});

    do_sample_y_bar = get_argument({'sample_y_bar', 'sample_y_bar'}, false, varargin{:});

    show_individuals = get_argument({'show_individual', 'show_individuals'}, [], varargin{:});

    %% Initialise data variables
    X_norm = cell(size(X));
    X_train = cell(size(X));
    X_train_norm = cell(size(X));
    y_train = cell(size(y));
    
    % Initialise output variables
    predictions = cell(size(y));

    p = NaN; % Predictors
    m_all = numel(X_train); % Participants

    f_show_individual = cell(numel(show_individuals), 1);
    
    for i = 1:numel(X)
        X_train{i} = X{i}(training_elements{i}, :);
        y_train{i} = y{i}(training_elements{i});

        if normalise
            X_train_mean = mean(X_train{i}, 1);
            X_train_std = std(X_train{i}, 1);
        else
            X_train_mean = zeros(1, size(X_train{i}, 2));
            X_train_std = ones(1, size(X_train{i}, 2));
        end

        X_norm{i} = (X{i} - repmat(X_train_mean, size(X{i}, 1), 1)) ./ repmat(X_train_std, size(X{i}, 1), 1);
        X_train_norm{i} = (X_train{i} - repmat(X_train_mean, size(X_train{i}, 1), 1)) ./ repmat(X_train_std, size(X_train{i}, 1), 1);

        predictions{i} = NaN(size(X{i}, 1), sweeps);

        if isnan(p)
            p = size(X_train{i}, 2);
        else
            assert(p == size(X_train{i}, 2), 'The number of predictors for individual %i does not match previous individuals.', i);
        end
    end

    if normalise || do_sample_y_bar
        y_bar = mean(vertcat(y_train{1:numel(X)}));
    else
        y_bar = 0;
    end

    % Initialise output variables
    psi_sampled = NaN(p, sweeps);
    sigma_squared_sampled = NaN(1, sweeps);
    y_bar_sampled = NaN(1, sweeps);

    % Initialise model variables
    sigma_squared = 1;
    tau_psi_squared = ones(p, 1);

    str = '';

    for sw = 1:sweeps
        str_old = str;
        str = sprintf(['Sweep% ' num2str(length(num2str(sweeps)) + 1) 'i of %i (%.1f%%%%)\n'], sw, sweeps, (sw / sweeps) * 100);
        fprintf([repmat('\b', 1, length(str_old) - 1) str]);

        psi = sample_psi(sigma_squared, tau_psi_squared, X_train_norm, y_train, y_bar);

        sigma_squared = sample_sigma_squared(sigma_squared_a, sigma_squared_gamma, tau_psi_squared, psi, X_train_norm, y_train, y_bar);

        tau_psi_squared = sample_tau_psi_squared(sigma_squared, lambda_psi, psi);
        
        if do_sample_y_bar
            y_bar = sample_y_bar(y_bar_mu, y_bar_sigma, sigma_squared, psi, X_train_norm, y_train);
        end

        psi_sampled(:, sw) = psi;
        sigma_squared_sampled(sw) = sigma_squared;
        y_bar_sampled(sw) = y_bar;

        for i=1:m_all
            predictions{i}(:, sw) = X_norm{i} * psi + y_bar;
        end

        if ~isempty(show_individuals)
            for si_i = 1:numel(show_individuals)
                if isempty(f_show_individual{si_i}) || ~ishandle(f_show_individual{si_i})
                    f_show_individual{si_i} = figure;
                else
                    figure(f_show_individual{si_i});
                end

                i = show_individuals(si_i);
                
                plot_predictions(y{i}, predictions{i}, varargin{:})
            end
            pause(0.1);
        end
    end

    fprintf(repmat('\b', 1, length(str) - 1));

    function sample = sample_tau_psi_squared(sigma_squared, lambda_psi, psi)
        tau_mu = sqrt(((lambda_psi ^ 2) * sigma_squared) ./ (psi .^ 2));
        tau_lambda = lambda_psi ^ 2;

        sample = NaN(p, 1);
        
        for m = 1:p
            pd = makedist('InverseGaussian', 'mu', tau_mu(m), 'lambda', tau_lambda);
            sample_inverse = random(pd, 1);

            sample(m) = 1 / sample_inverse;
        end
    end

    function sample = sample_psi(sigma_squared, tau_psi_squared, X, y, y_bar)
        assert(numel(X) == numel(y));
        
        m_group = numel(X);

        D_psi = diag(tau_psi_squared);

        A = inv(D_psi);
        B = zeros(1, p);
        
        for m = 1:m_group
            A = A + (X{m}' * X{m});
            B = B + ((y{m} - y_bar)' * X{m});
        end

        sample = mvnrnd(A \ B', inv(A) * sigma_squared)'; %#ok<MINV>
    end

    function sample = sample_sigma_squared(sigma_squared_a, sigma_squared_gamma, tau_psi_squared, psi, X, y, y_bar)
        assert(numel(X) == numel(y));
        
        m_group = numel(X);
        
        sigma_square_shape = 0;
        sigma_square_scale = 0;

        for m = 1:m_group
            if do_sample_y_bar
                sigma_square_shape = sigma_square_shape + (numel(y{m}) / 2);
            else
                sigma_square_shape = sigma_square_shape + ((numel(y{m}) - 1) / 2);
            end
            sigma_square_scale = sigma_square_scale + (((y{m} - y_bar - (X{m} * psi))' * (y{m} - y_bar - (X{m} * psi))) / 2);
        end

        D_psi = diag(tau_psi_squared);
        
        sigma_square_shape = sigma_square_shape + (p / 2) + sigma_squared_a;
        sigma_square_scale = sigma_square_scale + (((psi' / D_psi) * psi) / 2) + sigma_squared_gamma;

        sample = 1 / gamrnd(sigma_square_shape, 1 / sigma_square_scale);
    end

    function sample = sample_y_bar(y_bar_mu, y_bar_sigma, sigma_squared, psi, X, y)
        assert(numel(X) == numel(y));
        
        m_group = numel(X);
        
        sum_A = 0;
        sum_B = 0;

        for m = 1:m_group
            sum_A = sum_A + numel(y{m});
            sum_B = sum_B + sum(y{m} - X{m} * psi);
        end

        y_bar_sigma_squared = y_bar_sigma ^ 2;

        y_bar_mean = ((sigma_squared * y_bar_mu) + (y_bar_sigma_squared * sum_B)) / (sigma_squared + (y_bar_sigma_squared * sum_A));
        y_bar_std = sqrt((sigma_squared * y_bar_sigma_squared) / (sigma_squared + (y_bar_sigma_squared * sum_A)));

        sample = normrnd(y_bar_mean, y_bar_std);
    end
end
